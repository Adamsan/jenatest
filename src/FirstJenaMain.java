
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolutionMap;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.rdf.model.impl.LiteralImpl;
import com.hp.hpl.jena.sparql.modify.request.UpdateModify;
import com.hp.hpl.jena.update.Update;
import com.hp.hpl.jena.update.UpdateAction;
import com.hp.hpl.jena.update.UpdateExecutionFactory;
import com.hp.hpl.jena.update.UpdateFactory;
import com.hp.hpl.jena.update.UpdateRequest;
import com.hp.hpl.jena.vocabulary.VCARD;

public class FirstJenaMain {
	public static void printModel(Model m){
		System.out.println("\n--------------");
		m.write(System.out,"RDF/XML");
		System.out.println("--------------");
		//m.write(System.out,"TURTLE");
		//System.out.println("--------------");
		m.write(System.out,"N3");
		System.out.println("");
	}


	
	public static void main(String[] args) {
		//testOne();
		//testTwo();				
		//testThree();
		testFour();
	}

	private static void testFour() {
		try(InputStream in = new FileInputStream(new File("data.rdf"))) {
			
			Model m = ModelFactory.createMemModelMaker().createDefaultModel();
			m.read(in, null);
			
			printModel(m);
			
			String queryString = 
					"PREFIX cd:<http://www.recshop.fake/cd#> "+
					"SELECT ?artist "+
					"WHERE { "+
					"?disk cd:country \"UK\" . "+
					"?disk cd:artist ?artist . "+
					"}";
			Query query = QueryFactory.create(queryString);
			QueryExecution qe = QueryExecutionFactory.create(query, m);
			ResultSet rs= qe.execSelect();
			ResultSetFormatter.out(System.out,rs,query);
			
			String queryString2 = 
					"PREFIX cd:<http://www.recshop.fake/cd#> "+
					"SELECT ?disk ?artist ?country "+
					"WHERE { "+
					"?disk cd:country ?country . "+
					"?disk cd:artist ?artist . "+
					"}";
			Query query2 = QueryFactory.create(queryString2);		
			QuerySolutionMap qsm = new QuerySolutionMap();
			qsm.add("country", m.createLiteral("UK"));
			QueryExecution qe2 = QueryExecutionFactory.create(query2, m);
			qe2.setInitialBinding(qsm);
			ResultSet rs2= qe2.execSelect();
			ResultSetFormatter.out(System.out,rs2,query);
			
			String deleteString = 
					"DELETE DATA {?disk http://www.recshop.fake/cd#country \"USA\"}";
			//Query delquery = QueryFactory.create(deleteString);
			//QueryExecution delexec = QueryExecutionFactory.create(delquery,m);
			//UpdateAction.parseExecute(deleteString, m);
			//UpdateRequest ur = UpdateFactory.create(deleteString);
			//UpdateModify um = new UpdateModify();
			
			UpdateAction.parseExecute(" " +
					"DELETE DATA {" +
					"<http://www.recshop.fake/cd/Hide your heart> " +
					"<http://www.recshop.fake/cd#country> " +
					"USA" +
					"}", m);
			
			
			printModel(m);
			
		} catch (IOException e) {e.printStackTrace();}				
	}



	public static void testOne(){
		Model m = ModelFactory.createDefaultModel();
		String NS = "http://example.com/test/";
		Resource r = m.createResource(NS+"r");
		Property p = m.createProperty(NS+"p");
		r.addProperty(p, "Hello World",XSDDatatype.XSDstring);
		
		printModel(m);
		
	}
	public static void testTwo(){
		String personURI = "http://somewhere.com/JohnSmith";
		String fullName = "John Smith";
		Model model = ModelFactory.createDefaultModel();
		Resource johnSmith = model.createResource(personURI);
		johnSmith.addProperty(VCARD.FN, fullName);
		johnSmith.addProperty(com.hp.hpl.jena.vocabulary.DC.creator, fullName);
		
		printModel(model);
		
	}
	private static void testThree() {
		Model m = ModelFactory.createDefaultModel();
		String personURI = "http://somewhere/JohnSmith";
		String givenName = "John";
		String familyName = "Smith";
		String name = givenName + " " + familyName;
		Resource johnsmith = m.createResource(personURI);
		Resource emptyRS = m.createResource();
		emptyRS.addProperty(VCARD.Given, givenName);
		emptyRS.addProperty(VCARD.Family, familyName);
		johnsmith.addProperty(VCARD.FN,name);
		johnsmith.addProperty(VCARD.N, emptyRS);
		
		printModel(m);
		
		StmtIterator iter = m.listStatements();
		while(iter.hasNext()){
			Statement stmt = iter.nextStatement();
			
			Resource subject = stmt.getSubject();
			Property predicate = stmt.getPredicate();
			RDFNode object = stmt.getObject();

			System.out.println(subject.toString() + " ---" + predicate.getLocalName().toString() +"--> " + object.toString());
			
		}
		
	}	

}
